package cat712.todo.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * Класс для тетирования объектов типа Task
 * @version 1.0
 */
public class TaskTest {
    private static Task task;

    @BeforeAll
    public static void initAll() {
        ListTasks list = new ListTasks(1, "Список дел");
        task = new Task(1, "Купить пиццу", false, list);
    }

    @Test
    @DisplayName("Равны ли значения объекта таким же константным значениям")
    public void equalsValues() {
        ListTasks list = new ListTasks(1, "Список дел");

        Assertions.assertTrue((task.getId() == 1) && task.getName().equals("Купить пиццу") &&
                (task.isDone() == false) && task.getListTasks().equals(list));
    }

    @Test
    @DisplayName("Равны ли 2 объекта с одинаковыми значениями")
    public void equalsCopyObject() {
        ListTasks copyList = new ListTasks(1, "Список дел");
        Task copyTask = new Task(1, "Купить пиццу", false, copyList);

        Assertions.assertTrue(copyTask.equals(task));
    }

    @Test
    @DisplayName("Не равны ли два объекта с разными значениями")
    public void notEqualsCopyObjectWithOtherValues() {
        ListTasks anotherList = new ListTasks(2, "Покупки");
        Task anotherTask = new Task(345, "Помидор", true, anotherList);

        Assertions.assertFalse(task.equals(anotherTask));
    }

    @Test
    @DisplayName("Равны ли исходный и клонируемый объект")
    public void equalsCloneObject() {
        try {
            Task cloneTask = task.clone();
            Assertions.assertTrue(task.equals(cloneTask));
        } catch (CloneNotSupportedException ex) {
            System.err.println("Ошибка клонирования объекта типа Task: " + ex.getMessage());
        }
    }
}
