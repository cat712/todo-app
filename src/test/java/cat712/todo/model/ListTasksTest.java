package cat712.todo.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * Класс для тестирования объектов типа ListTasks
 * @version 1.0
 */
public class ListTasksTest {
    private final ListTasks list = new ListTasks(1, "Список дел");

    @Test
    @DisplayName("Равны ли значения объекта таким же константным значениям")
    public void equalsValues() {
        Assertions.assertTrue((list.getId() == 1) && list.getName().equals("Список дел"));
    }

    @Test
    @DisplayName("Равны ли 2 объекта с одинаковыми значениями")
    public void equalsCopyObject() {
        ListTasks copyList = new ListTasks(1, "Список дел");

        Assertions.assertTrue(list.equals(copyList));
    }

    @Test
    @DisplayName("Не равны ли два объекта с разными значениями")
    public void notEqualsCopyObjectWithOtherValues() {
        ListTasks anotherList = new ListTasks(2, "Покупки");

        Assertions.assertFalse(list.equals(anotherList));
    }

    @Test
    @DisplayName("Равны ли исходный и клонируемый объект")
    public void equalsCloneObject() {
        try {
            ListTasks cloneList = list.clone();
            Assertions.assertTrue(list.equals(cloneList));
        } catch (CloneNotSupportedException ex) {
            System.err.println("Ошибка клонирования объекта типа ListTasks: " + ex.getMessage());
        }
    }
}
